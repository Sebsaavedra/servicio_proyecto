# -*- coding: utf-8 -*-
from odoo import models, fields, api


class servicio_proyecto(models.Model):
    _name = 'servicio_proyecto.servicio_proyecto'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _description = "Servicio o Proyecto"
    _rec_name = 'servicio_proyecto_id'
    _order = 'sequence'

    nombre_servicio_proyecto = fields.Char(string="Servicio o Proyecto", track_visibility='onchange', index=True)
    tipo_servicio_proyecto = fields.Selection([('no_operacional', 'No Operacional'),('operacional_basal', 'Operacional Basal'),('operacional_discrecional', 'Operacional Discrecional')],string="Tipo de Servicio o Proyecto", index=True, track_visibility='onchange' )
    servicio_proyecto_id = fields.Many2one('servicio_proyecto', string='Servicio o Proyecto')


    message_attachment_count = fields.Integer(string="Conteo de archivos adjuntos", track_visibility='onchange', index=True)
    message_channel_ids = fields.Many2many('mail.channel', string="Seguidores (Canales)", track_visibility='onchange', index=True)
    message_follower_ids = fields.One2many('mail.followers', 'res_id', track_visibility='onchange', index=True)
    message_ids = fields.One2many('mail.message', 'res_id', track_visibility='onchange', index=True)
    message_main_attachment_id = fields.Many2one('ir.attachment', track_visibility='onchange', index=True)


@api.multi
@api.returns('mail.message', lambda value: value.id)
def message_post(self, **kwargs):
    if self.env.context.get('mark_rfq_as_sent'):
        self.filtered(lambda o: o.state == 'draft').write({'state': 'sent'})
    return super(PurchaseOrder, self.with_context(mail_post_autofollow=True)).message_post(**kwargs)

